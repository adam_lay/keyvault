﻿namespace KeyVault.Services

open System.Security.Cryptography
open System.IO

type ISymmetricEncryptionService =
  abstract member GenerateKey : unit -> byte[]
  abstract member GenerateIV : unit -> byte[]
  abstract member Encrypt : plainText : string * key : byte[] * iv : byte[] -> byte[]
  abstract member Decrypt : cipher : byte[] * key : byte[] * iv : byte[] -> string

type SymmetricEncryptionService () =
  interface ISymmetricEncryptionService with
    member this.GenerateKey() = Aes.Create().Key
    member this.GenerateIV() = Aes.Create().IV
    
    member this.Encrypt(plainText: string, key: byte [], iv: byte []) : byte[] = 
      use aes = Aes.Create()
      use encryptor = aes.CreateEncryptor(key, iv)
      use ms = new MemoryStream()
      use cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write)
      use sw = new StreamWriter(cs)
      plainText |> sw.Write
      ms.ToArray()

    member this.Decrypt(cipher : byte[], key : byte[], iv : byte[]) : string =
      use aes = Aes.Create()
      use decryptor = aes.CreateDecryptor(key, iv)
      use ms = new MemoryStream(cipher)
      use cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read)
      use sr = new StreamReader(cs)
      sr.ReadToEnd()
