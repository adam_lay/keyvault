﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyVault.Domain.Users
{
  public interface IUserRepository
  {
    User GetUser(string username);
    IEnumerable<User> GetUsers();

    void Save(User user);

    void Delete(int userId);
  }
}
