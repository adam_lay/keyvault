﻿using KeyVault.Data.Users;
using KeyVault.Domain.VaultPasswords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeyVault.Data.VaultPasswords
{
  public class VaultPasswordRepository : IVaultPasswordRepository
  {
    private readonly KeyVaultContext _dbContext;

    public VaultPasswordRepository(KeyVaultContext dbContext)
    {
      _dbContext = dbContext;
    }

    public VaultPassword GetVaultPassword(int vaultPasswordId)
    {
      return _dbContext.VaultPasswords
        .FirstOrDefault(vp => vp.Id == vaultPasswordId);
    }

    public IEnumerable<VaultPassword> GetVaultPasswords()
    {
      return _dbContext.VaultPasswords;
    }

    public IEnumerable<VaultPassword> GetVaultPasswordsForUser(string username)
    {
      return _dbContext.VaultPasswords
        .Where(vp => string.Compare(vp.Username, username, true) == 0);
    }

    public void Save(VaultPassword vaultPassword)
    {
      _dbContext.VaultPasswords.Add(vaultPassword);

      _dbContext.SaveChanges();
    }

    public void Delete(int userId)
    {
      throw new NotImplementedException();
    }
  }
}
