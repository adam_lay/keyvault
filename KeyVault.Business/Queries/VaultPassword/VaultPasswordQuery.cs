﻿using KeyVault.Domain.VaultPasswords;
using KeyVault.Services;

namespace KeyVault.Business.Queries.VaultPassword
{
  public class VaultPasswordQuery
  {
    private readonly IVaultPasswordRepository _vaultPasswordRepository;
    private readonly IAsymmetricEncryptionService _asymmetricEncryptionService;

    public VaultPasswordQuery(IVaultPasswordRepository vaultPasswordRepository, IAsymmetricEncryptionService asymmetricEncryptionService)
    {
      _vaultPasswordRepository = vaultPasswordRepository;
      _asymmetricEncryptionService = asymmetricEncryptionService;
    }

    public VaultPasswordResponse Execute(VaultPasswordRequest request)
    {
      Domain.VaultPasswords.VaultPassword vp = _vaultPasswordRepository.GetVaultPassword(request.VaultPasswordId);

      return new VaultPasswordResponse
      {
        Id = vp.Id,
        Name = vp.Name,
        Password = _asymmetricEncryptionService.Decrypt(request.PrivateKey, vp.PasswordCipher),
        ParentId = vp.ParentId
      };
    }
  }
}
