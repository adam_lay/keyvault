using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KeyVault.Web.Filters;
using KeyVault.Business.Queries.VaultPasswordsForUser;
using KeyVault.Business.Commands.InsertVaultPassword;
using KeyVault.Business.Commands.ShareVaultPassword;
using KeyVault.Business.Queries.User;
using KeyVault.Web.Models.Vault;
using KeyVault.Business.Queries.UserList;
using KeyVault.Business.Queries.AvailableToShare;

namespace KeyVault.Web.Controllers
{
  [AuthoriseKeyVault]
  public class VaultController : ControllerBase
  {
    private readonly VaultPasswordsForUserQuery _vaultPasswordsForUserQuery;
    private readonly InsertVaultPasswordCommand _insertVaultPassword;
    private readonly ShareVaultPasswordCommand _shareVaultPassword;
    private readonly UserQuery _userQuery;
    private readonly AvailableToShareQuery _availableToShareQuery;

    public VaultController(VaultPasswordsForUserQuery vaultPasswordsForUser,
      InsertVaultPasswordCommand insertVaultPassword, UserQuery userQuery,
      ShareVaultPasswordCommand shareVaultPassword, AvailableToShareQuery availableToShareQuery)
    {
      _vaultPasswordsForUserQuery = vaultPasswordsForUser;
      _insertVaultPassword = insertVaultPassword;
      _shareVaultPassword = shareVaultPassword;
      _userQuery = userQuery;
      _availableToShareQuery = availableToShareQuery;
    }

    [HttpGet]
    public IActionResult Index()
    {
      var pwds = _vaultPasswordsForUserQuery.Execute(new VaultPasswordsForUserRequest(AuthUser.Username, AuthUser.PrivateKey));

      return View(pwds.VaultPasswords.Select(vp => new VaultPasswordViewModel
      {
        Id = vp.Id,
        Name = vp.Name,
        Password = vp.Password,
        IsShared = vp.ParentId.HasValue
      }));
    }

    [HttpGet]
    public IActionResult Create()
    {
      return View(new VaultPasswordViewModel());
    }

    [HttpPost]
    public IActionResult Create(VaultPasswordViewModel model)
    {
      if (!ModelState.IsValid)
        return View(model);

      _insertVaultPassword.Execute(new InsertVaultPasswordRequest
      {
        Username = AuthUser.Username,
        PublicKey = AuthUser.PrivateKey,
        Name = model.Name,
        Password = model.Password
      });

      return RedirectToAction(nameof(Index));
    }

    [HttpGet]
    public IActionResult GetShareWithList(int id)
    {
      var users = _availableToShareQuery
        .Execute(new AvailableToShareRequest { Username = AuthUser.Username, VaultPasswordId = id });

      return Json(users);
    }

    [HttpPost]
    public IActionResult Share(ShareVaultPasswordViewModel model)
    {
      _shareVaultPassword.Execute(new ShareVaultPasswordRequest
      {
        OriginalVaultPasswordId = model.VaultPasswordId,
        ShareWith = model.ShareWith,
        PrivateKey = AuthUser.PrivateKey
      });

      return Ok();
    }
  }
}