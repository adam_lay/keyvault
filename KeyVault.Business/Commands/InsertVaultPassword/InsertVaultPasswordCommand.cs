﻿using KeyVault.Domain.VaultPasswords;
using KeyVault.Services;

namespace KeyVault.Business.Commands.InsertVaultPassword
{
  public class InsertVaultPasswordCommand
  {
    private readonly IVaultPasswordRepository _vaultPasswordRepository;
    private readonly IAsymmetricEncryptionService _asymmetricEncryptionService;
    
    public InsertVaultPasswordCommand(IVaultPasswordRepository vaultPasswordRepository, IAsymmetricEncryptionService asymmetricEncryptionService)
    {
      _vaultPasswordRepository = vaultPasswordRepository;
      _asymmetricEncryptionService = asymmetricEncryptionService;
    }

    public void Execute(InsertVaultPasswordRequest request)
    {
      byte[] password = _asymmetricEncryptionService.Encrypt(request.PublicKey, request.Password);

      _vaultPasswordRepository.Save(new VaultPassword
      {
        Username = request.Username,
        Name = request.Name,
        PasswordCipher = password
      });
    }
  }
}
