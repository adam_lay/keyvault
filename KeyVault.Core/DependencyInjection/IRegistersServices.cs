﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace KeyVault.Core.DependencyInjection
{
  internal interface IRegistersServices
  {
    void Register(IServiceCollection services, IConfigurationRoot configuration);
  }
}
