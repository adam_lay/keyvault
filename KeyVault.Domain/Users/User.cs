﻿using KeyVault.Domain.VaultPasswords;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KeyVault.Domain.Users
{
  public class User
  {
    [Key]
    [Required]
    public string Username { get; set; }
    public byte[] PasswordHash { get; set; } // Used when not using Windows Auth

    // Used to decrypt RSA PrivateKey
    public byte[] PrivateKeyIv { get; set; }
    
    /// <summary>
    /// For decrypting Symmetric Key. JSON Serialised RSAParameters. Encrypted with User's password + PrivateKeyIv.
    /// </summary>
    public byte[] PrivateKeyCipher { get; set; }

    /// <summary>
    /// For encrypting this user's Vault Passwords. JSON Serialised RSAParameters.
    /// </summary>
    public string PublicKey { get; set; }
    
    /// <summary>
    /// Collection of the User's Vault Passwords.
    /// </summary>
    public List<VaultPassword> VaultPasswords { get; set; }
  }
}
