using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KeyVault.Web.Models.Auth;
using KeyVault.Business.Queries.User;
using KeyVault.Business.Queries.AuthUser;
using KeyVault.Business.Commands.RegisterUser;
using KeyVault.Web.Extensions;

namespace KeyVault.Web.Controllers
{
  public class AuthController : ControllerBase
  {
    private readonly UserQuery _userQuery;
    private readonly AuthUserQuery _authUserQuery;
    private readonly RegisterUserCommand _registerUserCommand;

    public AuthController(UserQuery userQuery, AuthUserQuery authUserQuery, RegisterUserCommand registerUserCommand)
    {
      _userQuery = userQuery;
      _authUserQuery = authUserQuery;
      _registerUserCommand = registerUserCommand;
    }

    public IActionResult Login()
    {
      //if (!String.IsNullOrEmpty(User.Identity.Name))
      //{
      //  var result = _userQuery.Execute(new UserRequest(User.Identity.Name));

      //  if (result == null)
      //    return RedirectToAction("Register");
      //}

      return View(new LoginViewModel { Username = User.Identity.Name });
    }

    [HttpPost]
    public IActionResult Login(LoginViewModel model)
    {
      if (!ModelState.IsValid)
        return View(model);

      var existingUser = _userQuery.Execute(new UserRequest(model.Username));

      if (existingUser == null)
      {
        _registerUserCommand
          .Execute(new RegisterUserRequest(model.Username.ToLower(), model.Password));
      }

      if (!DoLogin(model.Username, model.Password))
        return View(new LoginViewModel { Username = User.Identity.Name });

      return RedirectToAction("Index", "Vault");
    }

    public IActionResult Register()
    {
      if (!String.IsNullOrEmpty(User.Identity.Name))
      {
        var result = _userQuery.Execute(new UserRequest(User.Identity.Name));

        if (result != null)
          return RedirectToAction("Login");
      }

      return View(new RegisterViewModel { Username = User.Identity.Name });
    }

    [HttpPost]
    public IActionResult Register(RegisterViewModel model)
    {
      if (!ModelState.IsValid)
        return View(model);

      _registerUserCommand
        .Execute(new RegisterUserRequest(model.Username.ToLower(), model.Password));

      // Log user in
      DoLogin(model.Username, model.Password);

      return RedirectToAction("Index", "Vault");
    }

    private bool DoLogin(string username, string password)
    {
      try
      {
        AuthUser = _authUserQuery.Execute(new AuthUserRequest(username, password));
      }
      catch (Exception)
      {
        // Password didn't work
        return false;
      }

      return true;
    }
  }
}