﻿using KeyVault.Business.Queries.User;
using KeyVault.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace KeyVault.Business.Queries.UserList
{
  public class UserListQuery
  {
    private readonly IUserRepository _userRepository;

    public UserListQuery(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public IEnumerable<UserResponse> Execute()
    {
      return _userRepository
        .GetUsers()
        .Select(u => new UserResponse
        {
          Username = u.Username,
          PublicKey = JsonConvert.DeserializeObject<RSAParameters>(u.PublicKey)
        })
        .ToList();
    }
  }
}
