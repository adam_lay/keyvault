﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using KeyVault.Domain.Users;
using KeyVault.Domain.VaultPasswords;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace KeyVault.Data.Users
{
  public class KeyVaultContext : DbContext
  {
    public KeyVaultContext(DbContextOptions<KeyVaultContext> options) : base(options) { }

    public DbSet<User> Users { get; set; }
    public DbSet<VaultPassword> VaultPasswords { get; set; }
  }

  public class DesignTimeKeyVaultContextFactory : IDesignTimeDbContextFactory<KeyVaultContext>
  {
    public KeyVaultContext CreateDbContext(string[] args)
    {
      IConfigurationRoot configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json")
        .Build();

      var builder = new DbContextOptionsBuilder<KeyVaultContext>();

      string connectionString = configuration.GetConnectionString("Default");

      builder.UseSqlServer(connectionString);

      return new KeyVaultContext(builder.Options);
    }
  }
}
