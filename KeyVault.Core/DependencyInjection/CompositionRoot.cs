﻿using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace KeyVault.Core.DependencyInjection
{
  public static class CompositionRoot
  {
    private static readonly List<IRegistersServices> _serviceRegistrars = new List<IRegistersServices>
    {
      new Data(),
      new Business()
    };

    public static void ComposeApplication(IServiceCollection services, IConfigurationRoot configuration)
    {
      services.AddAutoMapper();

      _serviceRegistrars.ForEach(r => r.Register(services, configuration));
    }
  }
}
