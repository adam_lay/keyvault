﻿using KeyVault.Business.Queries.AuthUser;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KeyVault.Web.Extensions;

namespace KeyVault.Web.Controllers
{
  public class ControllerBase : Controller
  {
    protected ISession Session => HttpContext.Session;

    protected AuthUserResponse AuthUser
    {
      get { return Session.Get<AuthUserResponse>(nameof(AuthUser)); }
      set { Session.Set<AuthUserResponse>(nameof(AuthUser), value); }
    }
  }
}
