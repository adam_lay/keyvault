﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Domain.VaultPasswords
{
  public interface IVaultPasswordRepository
  {
    VaultPassword GetVaultPassword(int vaultPasswordId);
    IEnumerable<VaultPassword> GetVaultPasswords();
    IEnumerable<VaultPassword> GetVaultPasswordsForUser(string username);

    void Save(VaultPassword vaultPassword);

    void Delete(int userId);
  }
}
