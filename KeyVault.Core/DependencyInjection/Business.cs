﻿using Microsoft.Extensions.DependencyInjection;
using KeyVault.Business.Services;
using KeyVault.Business.Commands.RegisterUser;
using KeyVault.Business.Commands.InsertVaultPassword;
using KeyVault.Business.Commands.ShareVaultPassword;
using KeyVault.Business.Queries.User;
using KeyVault.Business.Queries.AuthUser;
using KeyVault.Business.Queries.VaultPassword;
using KeyVault.Business.Queries.VaultPasswordsForUser;
using KeyVault.Business.Queries.UserList;
using KeyVault.Business.Queries.AvailableToShare;
using KeyVault.Services;
using Microsoft.Extensions.Configuration;

namespace KeyVault.Core.DependencyInjection
{
  internal class Business : IRegistersServices
  {
    public void Register(IServiceCollection services, IConfigurationRoot configuration)
    {
      // Commands
      services.AddSingleton<RegisterUserCommand>();
      services.AddSingleton<InsertVaultPasswordCommand>();
      services.AddSingleton<ShareVaultPasswordCommand>();

      // Queries
      services.AddSingleton<UserQuery>();
      services.AddSingleton<UserListQuery>();
      services.AddSingleton<AvailableToShareQuery>();
      services.AddSingleton<AuthUserQuery>();
      services.AddSingleton<VaultPasswordQuery>();
      services.AddSingleton<VaultPasswordsForUserQuery>();

      // Services
      services.AddSingleton<IHashingService, HashingService>();
      services.AddSingleton<ISymmetricEncryptionService, SymmetricEncryptionService>();
      services.AddSingleton<IAsymmetricEncryptionService, AsymmetricEncryptionService>();
      services.AddSingleton<IAuthenticationService, WindowsAuthenticationService>();
    }
  }
}
