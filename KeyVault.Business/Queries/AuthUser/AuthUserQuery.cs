﻿using KeyVault.Business.Services;
using KeyVault.Domain.Users;
using KeyVault.Services;

namespace KeyVault.Business.Queries.AuthUser
{
  public class AuthUserQuery
  {
    private readonly IUserRepository _userRepository;
    private readonly ISymmetricEncryptionService _symmetricEncryptionService;
    private readonly IHashingService _hashingService;
    private readonly IAuthenticationService _authenticationService;

    public AuthUserQuery(IUserRepository userRepository, ISymmetricEncryptionService symmetricEncryptionService,
      IHashingService hashingService, IAuthenticationService authenticationService)
    {
      _userRepository = userRepository;
      _symmetricEncryptionService = symmetricEncryptionService;
      _hashingService = hashingService;
      _authenticationService = authenticationService;
    }

    public AuthUserResponse Execute(AuthUserRequest request)
    {
      if (!_authenticationService.ValidUser(request.Username, request.Password))
        return null;

      Domain.Users.User user = _userRepository.GetUser(request.Username);

      if (user == null)
        return null;

      byte[] privateKeyKey = _hashingService.GenerateAesKeyFromString(request.Password);
      string privateKey = _symmetricEncryptionService.Decrypt(user.PrivateKeyCipher, privateKeyKey, user.PrivateKeyIv);
      
      return new AuthUserResponse
      {
        Username = user.Username,
        PrivateKeyJson = privateKey
      };
    }
  }
}
