﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KeyVault.Business.Queries.VaultPasswordsForUser
{
  public class VaultPasswordsForUserRequest
  {
    public string Username { get; set; }
    public RSAParameters PrivateKey { get; set; }

    public VaultPasswordsForUserRequest(string username, RSAParameters privateKey)
    {
      Username = username;
      PrivateKey = privateKey;
    }
  }
}
