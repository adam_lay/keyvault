﻿namespace KeyVault.Business.Services
{
  public interface IAuthenticationService
  {
    bool ValidUser(string username, string password);
  }
}
