﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KeyVault.Business.Commands.InsertVaultPassword
{
  public class InsertVaultPasswordRequest
  {
    public string Username { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public RSAParameters PublicKey { get; set; }
  }
}
