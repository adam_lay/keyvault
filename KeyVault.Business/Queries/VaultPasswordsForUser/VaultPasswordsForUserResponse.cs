﻿using KeyVault.Business.Queries.VaultPassword;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Queries.VaultPasswordsForUser
{
  public class VaultPasswordsForUserResponse
  {
    public IEnumerable<VaultPasswordResponse> VaultPasswords { get; set; }
  }
}
