﻿using KeyVault.Domain.Users;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeyVault.Data.Users
{
  public class UserRepository : IUserRepository
  {
    private readonly KeyVaultContext _dbContext;

    public UserRepository(KeyVaultContext dbContext)
    {
      _dbContext = dbContext;
    }

    public User GetUser(string username)
    {
      return _dbContext.Users
        .FirstOrDefault(u => string.Compare(u.Username, username, true) == 0);
    }

    public IEnumerable<User> GetUsers()
    {
      return _dbContext.Users;
    }

    public void Save(User user)
    {
      _dbContext.Users.Add(user);

      _dbContext.SaveChanges();
    }

    public void Delete(int userId)
    {
      throw new NotImplementedException();
    }
  }
}
