﻿namespace KeyVault.Business.Queries.AuthUser
{
  public class AuthUserRequest
  {
    public string Username { get; set; }
    public string Password { get; set; }

    public AuthUserRequest(string username, string password)
    {
      Username = username;
      Password = password;
    }
  }
}
