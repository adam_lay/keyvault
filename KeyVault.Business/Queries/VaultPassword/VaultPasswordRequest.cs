﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KeyVault.Business.Queries.VaultPassword
{
  public class VaultPasswordRequest
  {
    public int VaultPasswordId { get; set; }
    public RSAParameters PrivateKey { get; set; }
  }
}
