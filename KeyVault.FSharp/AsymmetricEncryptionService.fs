namespace KeyVault.Services

open System.Security.Cryptography
open System.Text

type RSAKeyPair = {
  PublicKey : RSAParameters
  PrivateKey : RSAParameters
}

type IAsymmetricEncryptionService =
  abstract member GenerateNewKeys : unit -> RSAKeyPair
  abstract member Encrypt : publicKey:RSAParameters -> data:string -> byte array
  abstract member Decrypt : privateKey:RSAParameters -> cipher:byte array -> string

type AsymmetricEncryptionService () =
  let KEY_SIZE = 0x2000

  interface IAsymmetricEncryptionService with
    member this.GenerateNewKeys() =
      use rsa = RSA.Create()
      rsa.KeySize <- KEY_SIZE
      { PublicKey = rsa.ExportParameters(false) ; PrivateKey = rsa.ExportParameters(true) }

    member this.Encrypt (publicKey:RSAParameters) (data:string) =
      use rsa = RSA.Create(publicKey)
      rsa.Encrypt(data |> Encoding.UTF8.GetBytes, RSAEncryptionPadding.OaepSHA512)

    member this.Decrypt (privateKey:RSAParameters) (cipher:byte array) =
      use rsa = RSA.Create(privateKey)
      rsa.Decrypt(cipher, RSAEncryptionPadding.OaepSHA512) |> Encoding.UTF8.GetString