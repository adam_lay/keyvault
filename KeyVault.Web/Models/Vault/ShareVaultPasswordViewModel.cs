﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeyVault.Web.Models.Vault
{
  public class ShareVaultPasswordViewModel
  {
    public int VaultPasswordId { get; set; }
    public string ShareWith { get; set; }
  }
}
