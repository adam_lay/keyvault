﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using KeyVault.Data.Users;

namespace KeyVault.Data.Migrations
{
    [DbContext(typeof(KeyVaultContext))]
    [Migration("20170201221803_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("KeyVault.Domain.Users.User", b =>
                {
                    b.Property<string>("Username")
                        .ValueGeneratedOnAdd();

                    b.Property<byte[]>("PasswordHash");

                    b.Property<byte[]>("PrivateKeyCipher");

                    b.Property<byte[]>("PrivateKeyIv");

                    b.Property<string>("PublicKey");

                    b.HasKey("Username");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("KeyVault.Domain.VaultPasswords.VaultPassword", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("ParentId");

                    b.Property<byte[]>("PasswordCipher")
                        .IsRequired();

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.HasIndex("Username");

                    b.ToTable("VaultPasswords");
                });

            modelBuilder.Entity("KeyVault.Domain.VaultPasswords.VaultPassword", b =>
                {
                    b.HasOne("KeyVault.Domain.Users.User")
                        .WithMany("VaultPasswords")
                        .HasForeignKey("Username");
                });
        }
    }
}
