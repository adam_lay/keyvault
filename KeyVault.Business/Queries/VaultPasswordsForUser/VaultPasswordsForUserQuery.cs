﻿using KeyVault.Business.Queries.VaultPassword;
using KeyVault.Business.Services;
using KeyVault.Domain.VaultPasswords;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Queries.VaultPasswordsForUser
{
  public class VaultPasswordsForUserQuery
  {
    private readonly IVaultPasswordRepository _vaultPasswordRepository;
    private readonly VaultPasswordQuery _getVaultPasswordQuery;

    public VaultPasswordsForUserQuery(IVaultPasswordRepository vaultPasswordRepository, VaultPasswordQuery getVaultPasswordQuery)
    {
      _vaultPasswordRepository = vaultPasswordRepository;
      _getVaultPasswordQuery = getVaultPasswordQuery;
    }

    public VaultPasswordsForUserResponse Execute(VaultPasswordsForUserRequest request)
    {
      return new VaultPasswordsForUserResponse
      {
        VaultPasswords = _vaultPasswordRepository
          .GetVaultPasswordsForUser(request.Username)
          .Select(vp => _getVaultPasswordQuery.Execute(new VaultPasswordRequest
          {
            VaultPasswordId = vp.Id,
            PrivateKey = request.PrivateKey
          }))
          .ToList()
      };
    }
  }
}
