﻿using KeyVault.Business.Queries.User;
using KeyVault.Domain.VaultPasswords;
using KeyVault.Services;

namespace KeyVault.Business.Commands.ShareVaultPassword
{
  public class ShareVaultPasswordCommand
  {
    private readonly IVaultPasswordRepository _vaultPasswordRepository;
    private readonly IAsymmetricEncryptionService _asymmetricEncryptionService;
    private readonly UserQuery _userQuery;

    public ShareVaultPasswordCommand(IVaultPasswordRepository vaultPasswordRepository,
      IAsymmetricEncryptionService asymmetricEncryptionService, UserQuery userQuery)
    {
      _vaultPasswordRepository = vaultPasswordRepository;
      _asymmetricEncryptionService = asymmetricEncryptionService;
      _userQuery = userQuery;
    }

    public void Execute(ShareVaultPasswordRequest request)
    {
      UserResponse toShareWith = _userQuery.Execute(new UserRequest(request.ShareWith));
      VaultPassword originalPassword = _vaultPasswordRepository.GetVaultPassword(request.OriginalVaultPasswordId);

      string passwordText = _asymmetricEncryptionService
        .Decrypt(request.PrivateKey, originalPassword.PasswordCipher);
      byte[] password = _asymmetricEncryptionService
        .Encrypt(toShareWith.PublicKey, passwordText);

      _vaultPasswordRepository.Save(new VaultPassword
      {
        Username = request.ShareWith,
        Name = originalPassword.Name,
        ParentId = originalPassword.Id,
        PasswordCipher = password
      });
    }
  }
}
