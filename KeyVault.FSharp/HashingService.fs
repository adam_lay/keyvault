﻿namespace KeyVault.Services

open System.Security.Cryptography
open System.Text
open BCrypt.Net

type IHashingService =
  abstract member GenerateAesKeyFromString : key:string -> byte array
  abstract member HashPassword : password:string -> byte array

type HashingService () =
  interface IHashingService with
    member this.GenerateAesKeyFromString (key:string) =
      use sha = SHA256.Create()
      key |> Encoding.UTF8.GetBytes |> sha.ComputeHash
    
    member this.HashPassword (password:string) =
      password |> BCrypt.HashPassword |> Encoding.UTF8.GetBytes
