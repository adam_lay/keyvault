﻿using KeyVault.Business.Commands.RegisterUser;
using KeyVault.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Core
{
  public class MappingProfile : AutoMapper.Profile
  {
    public MappingProfile()
    {
      CreateMap<RegisterUserRequest, User>();
    }
  }
}
