﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KeyVault.Business.Commands.ShareVaultPassword
{
  public class ShareVaultPasswordRequest
  {
    public int OriginalVaultPasswordId { get; set; }
    public string ShareWith { get; set; }
    public RSAParameters PrivateKey { get; set; } // Needed to decrypt the password, so it can be re-encrypted with new us'ers public key
  }
}
