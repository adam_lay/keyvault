﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KeyVault.Domain.VaultPasswords
{
  public class VaultPassword
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Username { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public byte[] PasswordCipher { get; set; }
    public int? ParentId { get; set; }
  }
}
