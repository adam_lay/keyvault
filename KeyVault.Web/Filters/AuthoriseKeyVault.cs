﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KeyVault.Web.Extensions;
using KeyVault.Business.Queries.AuthUser;
using Microsoft.AspNetCore.Mvc;

namespace KeyVault.Web.Filters
{
  public class AuthoriseKeyVault : Attribute, IAuthorizationFilter
  {
    public void OnAuthorization(AuthorizationFilterContext context)
    {
      var authUser = context.HttpContext.Session.Get<AuthUserResponse>("AuthUser");

      if (authUser == null)
        context.Result = new RedirectToActionResult("Login", "Auth", null);
    }
  }
}
