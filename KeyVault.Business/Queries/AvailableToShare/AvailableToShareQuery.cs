﻿using KeyVault.Domain.Users;
using KeyVault.Domain.VaultPasswords;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace KeyVault.Business.Queries.AvailableToShare
{
  public class AvailableToShareQuery
  {
    private readonly IUserRepository _userRepository;
    private readonly IVaultPasswordRepository _vaultPasswordRepository;

    public AvailableToShareQuery(IUserRepository userRepository,
      IVaultPasswordRepository vaultPasswordRepository)
    {
      _userRepository = userRepository;
      _vaultPasswordRepository = vaultPasswordRepository;
    }
    
    public IEnumerable<string> Execute(AvailableToShareRequest request)
    {
      // Get all potential users
      var toShareWith = _userRepository
        .GetUsers()
        .Where(u => string.Compare(u.Username, request.Username, true) != 0)
        .Select(u => u.Username);

      // Get usernames of everyone with access to the VP
      var ignoreUsers = _vaultPasswordRepository
        .GetVaultPasswords()
        .Where(vp => vp.Id == request.VaultPasswordId || vp.ParentId == request.VaultPasswordId)
        .Select(vp => vp.Username)
        .Distinct();

      // users that aren't in ignoredUsers
      return toShareWith.Except(ignoreUsers); //.Where(u => !ignoreUsers.Any(iu => string.Compare(iu, u, true) == 0));
    }
  }
}
