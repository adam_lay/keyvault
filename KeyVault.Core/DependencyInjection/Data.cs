﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using KeyVault.Data.Users;
using KeyVault.Domain.Users;
using KeyVault.Data.VaultPasswords;
using KeyVault.Domain.VaultPasswords;
using Microsoft.Extensions.Configuration;

namespace KeyVault.Core.DependencyInjection
{
  internal class Data : IRegistersServices
  {
    public void Register(IServiceCollection services, IConfigurationRoot configuration)
    {
      string connection = configuration.GetConnectionString("Default"); //@"Server=(localdb)\mssqllocaldb;Database=KeyVault_Test;Trusted_Connection=True;";

      services.AddDbContext<KeyVaultContext>(ops => ops.UseSqlServer(connection));
      
      services.AddSingleton<IUserRepository, UserRepository>();
      services.AddSingleton<IVaultPasswordRepository, VaultPasswordRepository>();
    }
  }
}
