﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace KeyVault.Business.Queries.User
{
  public class UserResponse
  {
    public string Username { get; set; }
    public RSAParameters PublicKey { get; set; }
  }
}
