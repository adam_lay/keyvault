﻿using KeyVault.Domain.Users;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace KeyVault.Business.Queries.User
{
  public class UserQuery
  {
    private readonly IUserRepository _userRepository;

    public UserQuery(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public UserResponse Execute(UserRequest request)
    {
      Domain.Users.User user = _userRepository.GetUser(request.Username);
      
      return user == null ? null : new UserResponse
      {
        Username = user.Username,
        PublicKey = JsonConvert.DeserializeObject<RSAParameters>(user.PublicKey)
      };
    }
  }
}
