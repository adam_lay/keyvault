﻿using KeyVault.Domain.Users;
using KeyVault.Domain.VaultPasswords;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Commands.EditVaultPassword
{
  public class EditVaultPasswordCommand
  {
    private readonly IUserRepository _userRepository;
    private readonly IVaultPasswordRepository _vaultPasswordRepository;

    public EditVaultPasswordCommand(IUserRepository userRepository,
      IVaultPasswordRepository vaultPasswordRepository)
    {
      _userRepository = userRepository;
      _vaultPasswordRepository = vaultPasswordRepository;
    }

    public void Execute(EditVaultPasswordRequest request)
    {

    }
  }
}
