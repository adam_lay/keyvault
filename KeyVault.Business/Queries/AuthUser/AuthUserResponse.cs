﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using KeyVault.Domain.Extensions;

namespace KeyVault.Business.Queries.AuthUser
{
  public class AuthUserResponse
  {
    public string Username { get; set; }
    public string PrivateKeyJson { get; set; }
    public RSAParameters PrivateKey => RSAExtensions.FromJsonString(PrivateKeyJson);
  }
}
