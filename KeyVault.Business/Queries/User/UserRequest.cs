﻿namespace KeyVault.Business.Queries.User
{
  public class UserRequest
  {
    public string Username { get; set; }

    public UserRequest(string username)
    {
      Username = username;
    }
  }
}
