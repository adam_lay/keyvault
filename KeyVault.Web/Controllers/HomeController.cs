﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using KeyVault.Web.Filters;

namespace KeyVault.Web.Controllers
{
  [AuthoriseKeyVault]
  public class HomeController : Controller
  {
    [HttpGet]
    public IActionResult Index()
    {
      return View();
    }

    [HttpGet]
    public IActionResult Error()
    {
      return View();
    }
  }
}
