﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Commands.EditVaultPassword
{
  public class EditVaultPasswordRequest
  {
    public int VaultPasswordId { get; set; }
  }
}
