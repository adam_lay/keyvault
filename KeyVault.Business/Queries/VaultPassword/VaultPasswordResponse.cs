﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Queries.VaultPassword
{
  public class VaultPasswordResponse
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public int? ParentId { get; set; }
  }
}
