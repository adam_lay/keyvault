﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Queries.AvailableToShare
{
  public class AvailableToShareRequest
  {
    public string Username { get; set; }
    public int VaultPasswordId { get; set; }
  }
}
