﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyVault.Business.Commands.RegisterUser
{
  public class RegisterUserRequest
  {
    public string Username { get; set; }
    public string Password { get; set; }

    public RegisterUserRequest(string username, string password)
    {
      Username = username;
      Password = password;
    }
  }
}
