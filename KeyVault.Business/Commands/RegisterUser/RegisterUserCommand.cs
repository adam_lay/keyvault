﻿using KeyVault.Domain.Users;
using KeyVault.Domain.Extensions;
using KeyVault.Services;

namespace KeyVault.Business.Commands.RegisterUser
{
  public class RegisterUserCommand
  {
    private readonly IUserRepository _userRepository;
    private readonly ISymmetricEncryptionService _symmetricEncryptionService;
    private readonly IAsymmetricEncryptionService _asymmetricEncryptionService;
    private readonly IHashingService _hashingService;

    public RegisterUserCommand(IUserRepository userRepository, ISymmetricEncryptionService symmetricEncryptionService,
      IAsymmetricEncryptionService asymmetricEncryptionService, IHashingService hashingService)
    {
      _userRepository = userRepository;
      _symmetricEncryptionService = symmetricEncryptionService;
      _asymmetricEncryptionService = asymmetricEncryptionService;
      _hashingService = hashingService;
    }

    public void Execute(RegisterUserRequest request)
    {
      // Get new public / private keys for user
      RSAKeyPair rsaKeys = _asymmetricEncryptionService
        .GenerateNewKeys();

      string publicKey = rsaKeys.PublicKey.ToJsonString();
      string privateKeyStr = rsaKeys.PrivateKey.ToJsonString();

      byte[] privateKeyAesKey = _hashingService.GenerateAesKeyFromString(request.Password);
      byte[] privateKeyAesIv = _symmetricEncryptionService.GenerateIV();

      byte[] privateKey = _symmetricEncryptionService.Encrypt(privateKeyStr, privateKeyAesKey, privateKeyAesIv);
      byte[] passwordHash = _hashingService.HashPassword(request.Password);

      //byte[] symmetricKey = _symmetricEncryptionService.GenerateKey();
      //byte[] symmetricKeyEncrypted = _asymmetricEncryptionService.Encrypt(symmetricKey, rsaKeys.publicKey);

      _userRepository.Save(new User
      {
        Username = request.Username,
        PasswordHash = passwordHash,
        PrivateKeyIv = privateKeyAesIv,
        PrivateKeyCipher = privateKey,
        PublicKey = publicKey
      });
    }
  }
}
